/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Gxz32
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFin = false;
        printWelcome();
        newGame();
        while (!isFin) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayers();
                isFin = true;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayers();
                isFin = true;
            }
            table.switchPlayer();
        }

    }

    private void printWelcome() {
        System.out.println("Welcome to game Tic-Tac-Toe!!");
    }

    private void printTable() {
        char[][] Board = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println("Is " + table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an empty row and column to place your mark!");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println("Congratz " +table.getCurrentPlayer().getSymbol() + " Win!!!");
    }

    private void printDraw() {
        System.out.println("Is Tie Game!");
    }
    
    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
